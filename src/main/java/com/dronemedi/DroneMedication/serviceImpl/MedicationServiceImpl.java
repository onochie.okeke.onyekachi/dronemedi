package com.dronemedi.DroneMedication.serviceImpl;

import com.dronemedi.DroneMedication.dto.MedicationDto;
import com.dronemedi.DroneMedication.exception.DMException;
import com.dronemedi.DroneMedication.model.Drone;
import com.dronemedi.DroneMedication.model.Medication;
import com.dronemedi.DroneMedication.pojo.MedicationPojo;
import com.dronemedi.DroneMedication.repository.DroneRepository;
import com.dronemedi.DroneMedication.repository.MedicationRepository;
import com.dronemedi.DroneMedication.service.MedicationService;
import com.dronemedi.DroneMedication.utils.enumUtil.DroneState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicationServiceImpl implements MedicationService {

    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    DroneRepository droneRepository;

    @Override
    public MedicationPojo addMedication(long droneId, MedicationDto medicationDto) throws Exception {
        long sumItems = 0;
        Drone drone = droneRepository.findById(droneId).orElse(new Drone());
        Medication medication = new Medication(medicationDto, drone);

        if (drone.getMedicationItems() != null) {
            for (Medication medication1: drone.getMedicationItems()) {
                sumItems += medication1.getWeight();
            }
        } else {
            throw new DMException("The Drone does not exist, Please add the drone first");
        }

        if ((drone.getStater() != DroneState.LOADED) & (sumItems < drone.getWeightLimit()) & (medication.getWeight() <= (drone.getWeightLimit() - sumItems))) {
            if ((sumItems+medication.getWeight()) < drone.getWeightLimit()) {

                if (drone.getBatteryCapacity() < 25) {
                    throw new Exception("Battery too low to be in LOADING state");
                }
                drone.setStater(DroneState.LOADING);
            } else {
                drone.setStater(DroneState.LOADED);
            }

            droneRepository.save(drone);
            return new MedicationPojo(medicationRepository.save(medication));
        } else {
            throw new Exception("Weight of medication is more than drone limit");
        }


    }

    @Override
    public List<MedicationPojo> getMedications() {
        List<MedicationPojo> medicationPojos = new ArrayList<>();
        for (Medication medication: medicationRepository.findAll()) {
            medicationPojos.add(new MedicationPojo(medication));
        }

        return medicationPojos;
    }
}
