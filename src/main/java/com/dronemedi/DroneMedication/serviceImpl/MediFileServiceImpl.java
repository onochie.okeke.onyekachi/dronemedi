package com.dronemedi.DroneMedication.serviceImpl;

import com.dronemedi.DroneMedication.exception.DMException;
import com.dronemedi.DroneMedication.model.MediFile;
import com.dronemedi.DroneMedication.model.Medication;
import com.dronemedi.DroneMedication.pojo.MediFilePojo;
import com.dronemedi.DroneMedication.repository.MediFileRepository;
import com.dronemedi.DroneMedication.repository.MedicationRepository;
import com.dronemedi.DroneMedication.service.MediFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MediFileServiceImpl implements MediFileService {

    @Autowired
    MediFileRepository mediFileRepository;

    @Autowired
    MedicationRepository medicationRepository;

    @Override
    public MediFilePojo addFile(long medicationId, MultipartFile multipartFile) throws Exception {
        Medication medication = medicationRepository.findById(medicationId).orElseThrow(Exception::new);

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        if (fileName.contains("..")) {
            throw new DMException("Invalid file name");
        } else {
            long fileSize = multipartFile.getSize();
            String fileType = multipartFile.getContentType();
            byte[] data = multipartFile.getBytes();

            MediFile mediFile = new MediFile(fileName, fileType, fileSize, data, medication);

            return new MediFilePojo(mediFileRepository.save(mediFile));
        }
    }
}
