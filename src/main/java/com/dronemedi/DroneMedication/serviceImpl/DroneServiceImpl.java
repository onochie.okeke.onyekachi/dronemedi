package com.dronemedi.DroneMedication.serviceImpl;

import com.dronemedi.DroneMedication.dto.DroneDto;
import com.dronemedi.DroneMedication.exception.DMException;
import com.dronemedi.DroneMedication.model.Drone;
import com.dronemedi.DroneMedication.model.Medication;
import com.dronemedi.DroneMedication.pojo.DronePojo;
import com.dronemedi.DroneMedication.pojo.MedicationPojo;
import com.dronemedi.DroneMedication.repository.DroneRepository;
import com.dronemedi.DroneMedication.repository.MedicationRepository;
import com.dronemedi.DroneMedication.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DroneServiceImpl implements DroneService {

    @Autowired
    DroneRepository droneRepository;

    @Autowired
    MedicationRepository medicationRepository;


    @Override
    public DronePojo registerDrone(DroneDto droneDto) {
        if (droneDto.getSerialNumber().length() > 100) {
            throw new DMException("Serial Number too long.");
        }

        Drone drone = new Drone(droneDto);
        return new DronePojo(droneRepository.save(drone));
    }

    @Override
    public List<DronePojo> allDrones() {
        List<DronePojo> dronePojos = new ArrayList<>();
        List<Drone> droneList = droneRepository.findAll();
        if (droneList == null) {
            return  null;
        } else {
            for (Drone drone: droneList) {
                dronePojos.add(new DronePojo(drone));
            }
        }

        return dronePojos;
    }

    @Override
    public List<MedicationPojo> medicationForDrone(long droneId) throws Exception {
        List<MedicationPojo> medicationPojos = new ArrayList<>();
        Drone drone = droneRepository.findById(droneId).orElseThrow(Exception::new);
        for (Medication medication: drone.getMedicationItems()) {
            medicationPojos.add(new MedicationPojo(medication));
        }

        return medicationPojos;
    }

    @Override
    public List<DronePojo> checkAvailableDrones() {
        List<DronePojo> dronePojoList =new ArrayList<>();
        List<Drone> droneList = droneRepository.findAvailableDrones();

        if (droneList.isEmpty()) {
            return  null;
        } else {
            for (Drone drone: droneList) {
                dronePojoList.add(new DronePojo(drone));
            }
        }

        return dronePojoList;
    }

    @Override
    public long checkBatterlevel(long droneId) throws Exception {
        Drone drone = droneRepository.findById(droneId).orElseThrow(Exception::new);
        return drone.getBatteryCapacity();
    }

    @Override
    public List<DronePojo> checkAllbatterLevels() {
        List<DronePojo> dronePojos = new ArrayList<>();
        List<Drone> droneList = droneRepository.findAll();

        if (droneList != null) {
            for (Drone drone: droneList) {
                dronePojos.add(new DronePojo(drone));
            }
        }

        return dronePojos;
    }
}
