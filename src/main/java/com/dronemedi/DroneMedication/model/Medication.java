package com.dronemedi.DroneMedication.model;

import com.dronemedi.DroneMedication.dto.MedicationDto;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="medication")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    private int weight;

    private String code;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference
    @JoinColumn(name = "drone_id", nullable = true)
    private Drone drone;

    @OneToMany(mappedBy = "medication", fetch = FetchType.EAGER)
    @JsonManagedReference
    @Nullable
    private List<MediFile> mediFileList;


    public Medication(MedicationDto medicationDto, Drone drone) {
        this.name = medicationDto.getName();
        this.weight = medicationDto.getWeight();
        this.code = medicationDto.getCode();
        this.drone = drone;
    }
}
