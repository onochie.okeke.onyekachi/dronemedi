package com.dronemedi.DroneMedication.model;

import com.dronemedi.DroneMedication.dto.DroneDto;
import com.dronemedi.DroneMedication.utils.enumUtil.DroneModel;
import com.dronemedi.DroneMedication.utils.enumUtil.DroneState;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "drone")
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String serialNumber;

    private DroneModel model;

    private int weightLimit;

    private int batteryCapacity;

    private DroneState stater;

    @OneToMany(mappedBy = "drone", fetch = FetchType.EAGER)
    @JsonManagedReference
    @Nullable
    private List<Medication> medicationItems;

    public Drone(DroneDto droneDto) {
        this.serialNumber = droneDto.getSerialNumber();
        this.model = droneDto.getModel();
        this.weightLimit = droneDto.getWeightLimit();
        this.batteryCapacity = droneDto.getBatteryCapacity();
        this.stater = DroneState.IDLE;
    }
}
