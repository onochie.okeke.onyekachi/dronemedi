package com.dronemedi.DroneMedication.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationDto {

    private String name;
    private int weight;
    private String code;
}
