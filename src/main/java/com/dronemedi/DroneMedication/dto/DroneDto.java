package com.dronemedi.DroneMedication.dto;

import com.dronemedi.DroneMedication.utils.enumUtil.DroneModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DroneDto {
    private String serialNumber;
    private DroneModel model;
    private int weightLimit;
    private int batteryCapacity;
}
