package com.dronemedi.DroneMedication.pojo;

import com.dronemedi.DroneMedication.model.MediFile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediFilePojo {
    private String fileNme;
    private String fileType;
    private long fileSize;
    private byte[] data;

    public MediFilePojo(MediFile mediFile) {
        this.fileNme = mediFile.getFileName();
        this.fileType = mediFile.getFileType();
        this.fileSize = mediFile.getFileSize();
        this.data = mediFile.getData();
    }
}
