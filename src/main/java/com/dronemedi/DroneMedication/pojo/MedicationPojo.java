package com.dronemedi.DroneMedication.pojo;

import com.dronemedi.DroneMedication.model.MediFile;
import com.dronemedi.DroneMedication.model.Medication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationPojo {

    private long id;
    private String name;
    private int weight;
    private String code;

    private List<MediFilePojo> mediFilePojoList = new ArrayList<>();

    public MedicationPojo(Medication medication) {
        this.id = medication.getId();
        this.name = medication.getName();
        this.weight = medication.getWeight();
        this.code = medication.getCode();

        if (medication.getMediFileList() != null) {
            for (MediFile mediFile: medication.getMediFileList()) {
                MediFilePojo mediFilePojo = new MediFilePojo(mediFile);
                mediFilePojoList.add(mediFilePojo);
            }
        }
    }
}
