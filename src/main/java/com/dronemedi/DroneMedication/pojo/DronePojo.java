package com.dronemedi.DroneMedication.pojo;

import com.dronemedi.DroneMedication.model.Drone;
import com.dronemedi.DroneMedication.model.Medication;
import com.dronemedi.DroneMedication.utils.enumUtil.DroneModel;
import com.dronemedi.DroneMedication.utils.enumUtil.DroneState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DronePojo {

    private long id;

    private String serialNumber;

    private DroneModel model;

    private int weightLimit;

    private int batteryCapacity;

    private DroneState state;

    private List<MedicationPojo> medicationPojoList = new ArrayList<>();

    public DronePojo(Drone drone) {
        this.id = drone.getId();
        this.serialNumber = drone.getSerialNumber();
        this.model = drone.getModel();
        this.weightLimit = drone.getWeightLimit();
        this.batteryCapacity = drone.getBatteryCapacity();
        this.state = drone.getStater();


        if (drone.getMedicationItems() != null) {
            for(Medication medication: drone.getMedicationItems()) {
                medicationPojoList.add(new MedicationPojo(medication));
            }
        }
    }
}
