package com.dronemedi.DroneMedication.service;

import com.dronemedi.DroneMedication.pojo.MediFilePojo;
import org.springframework.web.multipart.MultipartFile;

public interface MediFileService {
    MediFilePojo addFile(long medicationId, MultipartFile multipartFile) throws Exception;
}
