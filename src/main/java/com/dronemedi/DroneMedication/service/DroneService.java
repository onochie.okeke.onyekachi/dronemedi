package com.dronemedi.DroneMedication.service;

import com.dronemedi.DroneMedication.dto.DroneDto;
import com.dronemedi.DroneMedication.pojo.DronePojo;
import com.dronemedi.DroneMedication.pojo.MedicationPojo;

import java.util.List;

public interface DroneService {

    DronePojo registerDrone(DroneDto droneDto);

    List<DronePojo> allDrones();

    List<MedicationPojo> medicationForDrone(long droneId) throws Exception;

    List<DronePojo> checkAvailableDrones();

    long checkBatterlevel(long droneId) throws Exception;

    List<DronePojo> checkAllbatterLevels();
}
