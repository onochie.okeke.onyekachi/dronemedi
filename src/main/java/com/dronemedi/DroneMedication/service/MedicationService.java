package com.dronemedi.DroneMedication.service;

import com.dronemedi.DroneMedication.dto.MedicationDto;
import com.dronemedi.DroneMedication.pojo.MedicationPojo;

import java.util.List;

public interface MedicationService {

    MedicationPojo addMedication(long droneId, MedicationDto medicationDto) throws Exception;

    List<MedicationPojo> getMedications();
}
