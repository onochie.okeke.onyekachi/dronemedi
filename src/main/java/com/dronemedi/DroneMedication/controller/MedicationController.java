package com.dronemedi.DroneMedication.controller;

import com.dronemedi.DroneMedication.dto.MedicationDto;
import com.dronemedi.DroneMedication.pojo.MedicationPojo;
import com.dronemedi.DroneMedication.service.MedicationService;
import com.dronemedi.DroneMedication.utils.response.DMResponseCode;
import com.dronemedi.DroneMedication.utils.response.DMResponseEntity;
import com.dronemedi.DroneMedication.utils.response.DMResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/medication")
public class MedicationController {

    @Autowired
    MedicationService medicationService;

    @PostMapping("/add/{droneId}")
    public ResponseEntity<DMResponseEntity> addMedication(@PathVariable long droneId, @RequestBody MedicationDto medicationDto) throws Exception {
        if ((Pattern.matches("^[A-Za-z0-9_-]*$", medicationDto.getName()) != true) | (Pattern.matches("^[A-Z0-9_]*$", medicationDto.getCode()) != true)) {
            DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.BAD_REQUEST, DMResponseMessage.BAD_REQUEST_MESSAGE, null);
            return  ResponseEntity.ok(dmResponseEntity);
        }
        MedicationPojo medicationPojo = medicationService.addMedication(droneId, medicationDto);
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, medicationPojo);
        return  ResponseEntity.ok(dmResponseEntity);
    }

    @GetMapping("/get-medications")
    public ResponseEntity<DMResponseEntity> getMedications(){
        List<MedicationPojo> medicationPojoList = medicationService.getMedications();
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, medicationPojoList);
        return  ResponseEntity.ok(dmResponseEntity);
    }
}
