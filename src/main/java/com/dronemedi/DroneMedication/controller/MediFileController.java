package com.dronemedi.DroneMedication.controller;


import com.dronemedi.DroneMedication.pojo.MediFilePojo;
import com.dronemedi.DroneMedication.service.MediFileService;
import com.dronemedi.DroneMedication.utils.response.DMResponseCode;
import com.dronemedi.DroneMedication.utils.response.DMResponseEntity;
import com.dronemedi.DroneMedication.utils.response.DMResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/file")
public class MediFileController {

    @Autowired
    MediFileService mediFileService;

    @PostMapping("/addFile/{medicationId}")
    public ResponseEntity<DMResponseEntity> addFile(@PathVariable long medicationId, @RequestParam("file")MultipartFile multipartFile, HttpServletRequest httpServletRequest) throws Exception {
        MediFilePojo mediFilePojo = mediFileService.addFile(medicationId, multipartFile);
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, mediFilePojo);
        return ResponseEntity.ok(dmResponseEntity);
    }
}
