package com.dronemedi.DroneMedication.controller;

import com.dronemedi.DroneMedication.dto.DroneDto;
import com.dronemedi.DroneMedication.pojo.DronePojo;
import com.dronemedi.DroneMedication.pojo.MedicationPojo;
import com.dronemedi.DroneMedication.service.DroneService;
import com.dronemedi.DroneMedication.utils.response.DMResponseCode;
import com.dronemedi.DroneMedication.utils.response.DMResponseEntity;
import com.dronemedi.DroneMedication.utils.response.DMResponseMessage;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/drone")
public class DispatchController {

    @Autowired
    DroneService droneService;


    @PostMapping("/register-drone")
    public ResponseEntity<DMResponseEntity> registerDrone(@RequestBody DroneDto droneDto) {
        DronePojo dronePojo = droneService.registerDrone(droneDto);
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, dronePojo);
        return ResponseEntity.ok(dmResponseEntity);
    }

    @GetMapping("/getAllDrones")
    public ResponseEntity<DMResponseEntity> getAllDrones() {
        List<DronePojo> dronePojos = droneService.allDrones();
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, dronePojos);
        return ResponseEntity.ok(dmResponseEntity);
    }

    @GetMapping("/medication-for-drone/{droneId}")
    public ResponseEntity<DMResponseEntity> getMedicationForDrone(@PathVariable long droneId) throws Exception {
        List<MedicationPojo> medicationPojos = droneService.medicationForDrone(droneId);
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, medicationPojos);
        return ResponseEntity.ok(dmResponseEntity);
    }

    @GetMapping("/checkAvailableDrones")
    public ResponseEntity<DMResponseEntity> checkAvailableDrones(){
        List<DronePojo> dronePojos = droneService.checkAvailableDrones();
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, dronePojos);
        return ResponseEntity.ok(dmResponseEntity);
    }

    @GetMapping("/checkBatteryLevel/{droneId}")
    public ResponseEntity<DMResponseEntity> checkBatteryLevel(@PathVariable long droneId) throws Exception {
        long batteryLevel = droneService.checkBatterlevel(droneId);
        DMResponseEntity dmResponseEntity = new DMResponseEntity(DMResponseCode.SUCCESS, DMResponseMessage.SUCCESS_MESSAGE, batteryLevel);
        return ResponseEntity.ok(dmResponseEntity);
    }
}
