package com.dronemedi.DroneMedication.exception;

import java.util.function.Supplier;

public class DMException extends RuntimeException{
    public  DMException(String message) {
        super(message);
    }
}
