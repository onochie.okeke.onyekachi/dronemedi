package com.dronemedi.DroneMedication.repository;

import com.dronemedi.DroneMedication.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
