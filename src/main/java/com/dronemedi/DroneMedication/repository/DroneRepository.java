package com.dronemedi.DroneMedication.repository;

import com.dronemedi.DroneMedication.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

    @Override
    Optional<Drone> findById(Long aLong);

    @Query("SELECT d FROM Drone d WHERE d.stater = 0 OR d.stater = 1")
    List<Drone> findAvailableDrones();
}
