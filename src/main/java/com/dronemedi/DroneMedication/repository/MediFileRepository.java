package com.dronemedi.DroneMedication.repository;

import com.dronemedi.DroneMedication.model.MediFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MediFileRepository extends JpaRepository<MediFile, Long> {
}
