package com.dronemedi.DroneMedication.utils.enumUtil;

public enum DroneModel {
    LIGHTWEGHT,
    MIDDLEWEIGHT,
    CRUISERWEIGHT,
    HEAVYWEIGHT
}
