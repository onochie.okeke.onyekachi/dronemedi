package com.dronemedi.DroneMedication.utils;

import com.dronemedi.DroneMedication.pojo.DronePojo;
import com.dronemedi.DroneMedication.service.DroneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScheculeBatteryLevel {

    @Autowired
    DroneService droneService;

    Logger logger = LoggerFactory.getLogger(ScheculeBatteryLevel.class);

    @Scheduled(fixedRate = 3000)
    public void checkBatteryLevelCronjob () {
        List<DronePojo> dronePojoList = droneService.checkAllbatterLevels();

        logger.info("==============================================================================================");

        for (DronePojo dronePojo: dronePojoList) {
            logger.info("Battery level for Drone: ID => {} is {}", dronePojo.getId(), dronePojo.getBatteryCapacity());
        }

        logger.info("==============================================================================================");


    }
}
