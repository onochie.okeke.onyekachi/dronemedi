package com.dronemedi.DroneMedication.utils.response;

public class DMResponseMessage {
    public static String SUCCESS_MESSAGE = "Successful";
    public static String BAD_REQUEST_MESSAGE = "Invalid parameter";
}
