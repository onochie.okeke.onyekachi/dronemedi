package com.dronemedi.DroneMedication.utils.response;

public class DMResponseCode {
    public static int SUCCESS = 200;
    public static int BAD_REQUEST = 400;
}
