package com.dronemedi.DroneMedication.utils.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DMResponseEntity {
    int code;
    String message;
    Object data;
}
