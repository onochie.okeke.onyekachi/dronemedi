THIS IS THE README FILE THAT DESCRIBES THE OPERATION OF THE "DRONEMEDICATION" PROJECT

1. Clone the repository using the command - git clone https://gitlab.com/onochie.okeke.onyekachi/dronemedi.git
2. run - mvn clean
3. run - mvn install
4. Make sure the dependencies have been resolved
5. Run the application from your IDE
6. Make sure the project is running on localhost:7070
7. The API documentation is available at : https://documenter.getpostman.com/view/8494822/2s83tJGWHz
The APIs should be called in the following order:

a. RegisterDrone (Drone Directory) - to add a drone.
b. AddMedication (Medication Directory) - The ID from the drone response above should be passed as a path variable for this API call.

N:B - The name should only contain upper or lower case letters, dash, underscore or number, also the code should only contain upper case letters, dash, underscore or number.

c. AddFile (DroneMediFile Directory) - The ID from the Medication response above should be passed as a path variable for this API call. The body should be a form-data, with key value - file.

d. To get the medications attached to a particular drone: MedicationForADrone (Drone Directory): parse the DRONE ID as a path variable.

e. checkAvailableDrones (Drone Directory): This returns the drones that are either in IDLE or LOADING state.

f. checkDroneBatteryLevel (Drone Directory): parse the Drone ID as a path variable.

g. The drone table has been preloaded with 9 drone records. I left space for one so that a drone can be registered and it would add up to 10 drones.

The Audit log showing the battery percentage of all drone displays on the console as the application is running.



